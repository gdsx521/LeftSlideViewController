Pod::Spec.new do |s|

  s.name         = 'LeftSlideViewController'
  s.license      = 'MIT'
  s.version      = '1.0'
  s.summary      = 'IOS侧边栏'
  s.framework    = 'UIKit'
  s.requires_arc = true
  s.homepage     = 'https://gitlab.com/gdsx521/LeftSlideViewController'
  s.platform     = :ios,'8.0'
  s.source       = { :git => 'https://gitlab.com/gdsx521/LeftSlideViewController.git', :tag => '1.0' }
  s.author       = {'jie' => '1073894748@qq.com'}
  s.source_files  = 'LeftSlideViewController/*'

end
