//
//  AppDelegate.h
//  LeftSlideViewController
//
//  Copyright © 2016年 wdj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

